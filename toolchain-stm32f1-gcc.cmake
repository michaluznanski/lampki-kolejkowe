# set operating system, or rather lack of it
# its important, because it turns off all flags automatically added by cmake
# which on Windows system would be targeted for it
set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR arm)


find_program(ARM_GCC arm-none-eabi-gcc)
find_program(ARM_G++ arm-none-eabi-g++)
if (NOT ARM_GCC)
    message(SEND_ERROR ${ARM_GCC})
    message(FATAL_ERROR "Nie znaleziono armowego gcc")
endif()
if (NOT ARM_G++)
    message(FATAL_ERROR "Nie znaleziono armowego g++")
endif()

set(CMAKE_C_COMPILER ${ARM_GCC})
set(CMAKE_CXX_COMPILER ${ARM_G++})
set(CMAKE_TRY_COMPILE_TARGET_TYPE "STATIC_LIBRARY") # by ominąć sprawdzenie przykładowego programu, na czym się kładzie

add_compile_options("-mcpu=cortex-m3" "-mfloat-abi=soft" "-mthumb"
"-fsingle-precision-constant" "-Wdouble-promotion" "-Wfloat-conversion" "-ffast-math"
)
add_compile_definitions(${STM_VERSION})

# ustaw startup
set(STARTUP_FILE ${CMAKE_CURRENT_SOURCE_DIR}/toolchain_g4_gcc/startup_stm32g431xx.s)

if (WIN32)
    set(COMPILER_PATH "C:/Program Files/msys64/mingw64/")
else ()
    # w zasadzie powinienem użyć ścieżki zwróconej przez find_program chyba
    set(COMPILER_PATH "/usr/arm-none-eabi")
endif()
# czyli gdzie find_costam będzie zaczynać poszukiwania i szukać, można ustawić na więcej niż jedną
set(CMAKE_FIND_ROOT_PATH  
    ${COMPILER_PATH}
)

# nigdy nie używaj ustawionej powyżej ścieżki w find_program i pochodnych go używających
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# zawsze używaj ustawionej powyżej ścieżki w find_library
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
# zawsze używaj ustawionej powyżej ścieżki w find_path i find_file
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
# trzecia droga - both - najpierw szukaj z użyciem prefiksu(ustawionej ścieżki)
# jeśli nie wyjdzie to domyślnej ścieżki
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)


add_link_options(
    --specs=nosys.specs
    # -u_printf_float
    # -u_scanf_float
    # -nostartfiles
    LINKER:--gc-sections
    LINKER:--build-id
    -mcpu=cortex-m3 
    -mfloat-abi=soft 
    -mthumb
)