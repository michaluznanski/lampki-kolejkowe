#include "main.h"
#include "stm32f1xx_it.h"
#include "usart.h"
#include "spi.h"
#include "i2c.h"

#include "nRF24.hpp"
#include "RfidTransceiver.hpp"
#include "Callback.hpp"
#include "Utilities.hpp"
#include "Magnetometer.hpp"

#include <utility>

constexpr uint8_t KEY = 0xa7;

constexpr uint8_t TOGGLES_COUNT       = 60;
constexpr uint16_t TOGGLE_INTERVAL_ms = 500;

static inline void blinkNTimes(unsigned _times, unsigned _interTime_ms = 500)
{
    for (unsigned i = 0; i < _times * 2; ++i)
    {
        HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
        HAL_Delay(_interTime_ms);
    }
}

// można potem ew. dodać jeszcze watchdoga, by w razie czego nie utknął
static inline void ErrorStop(const char* _title, unsigned _lineNr)
{
#ifdef DEBUG
    do
    {
        log("\n\rError in line: %d\n\r", _lineNr);
        log(_title);
        log("\n\rApplication stopped!\n\r");
        blinkNTimes(5, 200);
    } while (false);
#else
    blinkNTimes(5, 200);
#endif
}

void the_mainest()
{
    log("Start program\n\r");
    blinkNTimes(3);

    log("Initialize radio\n\r");
    Communication::SpiMaster spi{hspi1};
    Radio::nRF24 radio{
        spi,
        Gpio::BinaryIO{SPI1_nRF24_CS_GPIO_Port, SPI1_nRF24_CS_Pin},
        Gpio::BinaryIO{RADIO_ENABLE_GPIO_Port, RADIO_ENABLE_Pin}};

    if (!radio.init())
    {
        ErrorStop("Radio's init failed", __LINE__);
    }
    else
    {
        log("Radio initialized!\n\r");
        blinkNTimes(2);
    }


    log("Receiver specific init\n\r");

    HAL_GPIO_WritePin(
        LAMP1_GPIO_Port,
        LAMP1_Pin,
        GPIO_PinState::GPIO_PIN_SET);    // set because of negative logic
    HAL_GPIO_WritePin(LAMP2_GPIO_Port, LAMP2_Pin, GPIO_PinState::GPIO_PIN_SET);

    if (!radio.openReadingPipe({0x00, 0x01, 0x02, 0x03, 0x04}, 1))
        ErrorStop("Opening reading pipe failed", __LINE__);
    if (!radio.setPowerAplification(
            Radio::nRF24::PALevel::dbm0))    // full power
        ErrorStop("Setting power failed", __LINE__);
    if (!radio.startListening())
        ErrorStop("Listeing start failed", __LINE__);

    log("Receiver init success\n\r");


    HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PinState::GPIO_PIN_SET);
    while (true)
    {

        auto radioCheck = radio.checkRxNotEmpty();
        if (radioCheck.first && radioCheck.second)
        {
            log("Message in fifo\n\r");
            auto data = radio.receiveData();
            if (!data.first)
            {
                log("Data receiving failed\n\r");
                continue;
            }
            else
            {
                log("Received data: %s", data.second.data());
            }
            if (data.second[0] == KEY)
            {
                log("Key accepted\n\r");
                if (!radio.stopListening())
                    log("Stopping radio error\n\r");
                HAL_GPIO_WritePin(LAMP1_GPIO_Port,
                                  LAMP1_Pin,
                                  GPIO_PinState::GPIO_PIN_RESET);
                HAL_GPIO_WritePin(LAMP2_GPIO_Port,
                                  LAMP2_Pin,
                                  GPIO_PinState::GPIO_PIN_SET);
                for (unsigned i = 0; i < TOGGLES_COUNT; ++i)
                {
                    HAL_GPIO_TogglePin(LAMP1_GPIO_Port, LAMP1_Pin);
                    HAL_GPIO_TogglePin(LAMP2_GPIO_Port, LAMP2_Pin);
                    HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
                    HAL_Delay(TOGGLE_INTERVAL_ms);
                }
                HAL_GPIO_WritePin(LAMP1_GPIO_Port,
                                  LAMP1_Pin,
                                  GPIO_PinState::GPIO_PIN_SET);
                HAL_GPIO_WritePin(LAMP2_GPIO_Port,
                                  LAMP2_Pin,
                                  GPIO_PinState::GPIO_PIN_SET);
                radio.flushRx();
                radio.clearFlags();
                log("Toggling ended\n\r");
                if (!radio.startListening())
                    log("Starting radio fail\n\r");
            }
            else
            {
                log("Key unknown\n\r");
            }
        }
        else if (!radioCheck.first)
        {
            log("Communication error!");
        }
    }
}
