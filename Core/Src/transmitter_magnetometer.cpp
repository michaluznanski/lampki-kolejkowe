#include "main.h"
#include "stm32f1xx_it.h"
#include "usart.h"
#include "spi.h"
#include "i2c.h"

#include "nRF24.hpp"
#include "RfidTransceiver.hpp"
#include "Callback.hpp"
#include "Utilities.hpp"
#include "Magnetometer.hpp"
#include "tan_table.h"

#include <utility>
#include <cmath>

constexpr unsigned THRESHOLD_ABS_VEC = 1000; // TBD
constexpr int ANG_DEG_MIN = 30; // TBD
constexpr int ANG_DEG_MAX = 60; // TBD

constexpr uint8_t KEY = 0xa7;

constexpr uint8_t RFID_ATTEMPTS_COUNT      = 10;
constexpr int64_t MIN_BUTTON_PRESS_TIME_MS = 500;
const uint8_t* RFID_CODE1                  = (const uint8_t*)("\x02"
                                                              "44002070C7D3"
                                                              "\x03");
const uint8_t* RFID_CODE2                  = (const uint8_t*)"\x02"
                                                             "2E00E5F90C3E"
                                                             "\x03";

static inline void blinkNTimes(unsigned _times, unsigned _interTime_ms = 500)
{
    for (unsigned i = 0; i < _times * 2; ++i)
    {
        HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
        HAL_Delay(_interTime_ms);
    }
}

// można potem ew. dodać jeszcze watchdoga, by w razie czego nie utknął
static inline void ErrorStop(const char* _title, unsigned _lineNr)
{
#ifdef DEBUG
    do
    {
        log("\n\rError in line: %d\n\r", _lineNr);
        log(_title);
        log("\n\rApplication stopped!\n\r");
        blinkNTimes(5, 200);
    } while (false);
#else
    blinkNTimes(5, 200);
#endif
}

auto magnet_vector_xy_abs(const Magnetometer::Vec3& magn_vec)
{
    return (abs(std::get<Magnetometer::X>(magn_vec)) + abs(std::get<Magnetometer::Y>(magn_vec))) / 2;
}

auto check_if_triggered(unsigned threshold, int vec_abs_val)
{
    return vec_abs_val > threshold;
}

auto check_if_direction_match(const Magnetometer::Vec3& v)
{
    auto angle_deg = atan(std::get<Magnetometer::X>(v), std::get<Magnetometer::Y>(v));
    if (angle_deg < ANG_DEG_MAX and angle_deg <= ANG_DEG_MIN)
    {
        return true;
    }
    return false;
}


void the_mainest()
{
    log("Start program\n\r");
    blinkNTimes(3);

    log("Initialize radio\n\r");
    Communication::SpiMaster spi{hspi1};
    Radio::nRF24 radio{
        spi,
        Gpio::BinaryIO{SPI1_nRF24_CS_GPIO_Port, SPI1_nRF24_CS_Pin},
        Gpio::BinaryIO{RADIO_ENABLE_GPIO_Port, RADIO_ENABLE_Pin}};

    if (!radio.init())
    {
        ErrorStop("Radio's init failed", __LINE__);
    }
    else
    {
        log("Radio initialized!\n\r");
        blinkNTimes(2);
    }

    log("Transmitter specific init\n\r");

    /* rfid init */
    log("Init rfid\n\r");
    RfidTransceiver<200> rfid{huart1, RFID_CODE1};
    // auto rfidCall = make_callback([&rfid]() { rfid.idleInterrupt(); });
    // register_uart1Idle(&rfidCall);
    // USART1->CR1 |= USART_CR1_IDLEIE | USART_CR1_UE; //
    // rfid.init();

    /* button init */
    log("Init button\n\r");
    // nothing to do

    /* magnet sensor init */
    log("Init magnets\n\r");
    auto magn = Magnetometer(hi2c1,
                             Magnetometer::Range::gauss2,
                             Magnetometer::Rate::hz25,
                             Magnetometer::Mode::continuous_conv);
    if (!magn.initialize())
        ErrorStop("Unable to initialize magnetometer", __LINE__);

    log("Opening writing pipe\n\r");
    if (!radio.openWritingPipe({0x00, 0x01, 0x02, 0x03, 0x04}))
        ErrorStop("Opening writing pipe failed", __LINE__);

    if (!radio.setPowerAplification(Radio::nRF24::PALevel::dbm0))
        ErrorStop("Setting power failed", __LINE__);

    if (!radio.stopListening())
        ErrorStop("Listening stop failed", __LINE__);

    if (!radio.enableTransmitWithoutAck())
        ErrorStop("No ack transmission enable failed", __LINE__);

    HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PinState::GPIO_PIN_SET);
    while (true)
    {

        /* MAGNET CODE */
        auto magVec = magn.getMagneticVector_bare();
        if (!magVec.first)
        {
            log("Read of magnetic vector failed\n");
            continue;
        }
        auto absVal = magnet_vector_xy_abs(magVec.second);
        if (check_if_triggered(THRESHOLD_ABS_VEC, absVal))
        {
            log("Strong magnetic field detected!");
            if (check_if_direction_match(magVec.second))
            {
                log("Vector match!");

            }
            
        }
        /* END OF MAGNET CODE */

    }
}

