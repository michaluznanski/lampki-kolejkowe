/*
 * mainer.cpp
 *
 *  Created on: Apr 23, 2022
 *      Author: Dell
 */
#include "main.h"
#include "stm32f1xx_it.h"
#include "usart.h"
#include "spi.h"
#include "i2c.h"

#include "nRF24.hpp"
#include "RfidTransceiver.hpp"
#include "Callback.hpp"
#include "Utilities.hpp"
#include "Magnetometer.hpp"

#include <utility>

#define TRANSMITTER
// #define RECEIVER
// #define MAGNETOMETER_TEST
#define MAGNETOMETER_TRIGGER

#if defined(MAGNETOMETER_TRIGGER) or defined(MAGNETOMETER_TEST)
    #define MAGNETOMETER
#endif

constexpr unsigned THRESHOLD_ABS_VEC = 1000; // TBD

constexpr uint8_t KEY = 0xa7;

#if defined(TRANSMITTER) and defined(RECEIVER)
#error "The device cannot be a transmitter and a receiver at once!"
#endif
#if (defined(RECEIVER) and defined(MAGNETOMETER_TEST)) or (defined(MAGNETOMETER_TRIGGER) and defined(RECEIVER))
#error "Magnetometer's only on transmitter"
#endif

#if defined(TRANSMITTER)
constexpr uint8_t RFID_ATTEMPTS_COUNT      = 10;
constexpr int64_t MIN_BUTTON_PRESS_TIME_MS = 500;
const uint8_t* RFID_CODE1                  = (const uint8_t*)("\x02"
                                                              "44002070C7D3"
                                                              "\x03");
const uint8_t* RFID_CODE2                  = (const uint8_t*)"\x02"
                                                             "2E00E5F90C3E"
                                                             "\x03";
#endif

#if defined(RECEIVER)
constexpr uint8_t TOGGLES_COUNT       = 60;
constexpr uint16_t TOGGLE_INTERVAL_ms = 500;
#endif

static inline void blinkNTimes(unsigned _times, unsigned _interTime_ms = 500)
{
    for (unsigned i = 0; i < _times * 2; ++i)
    {
        HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
        HAL_Delay(_interTime_ms);
    }
}

// można potem ew. dodać jeszcze watchdoga, by w razie czego nie utknął
static inline void ErrorStop(const char* _title, unsigned _lineNr)
{
#ifdef DEBUG
    do
    {
        log("\n\rError in line: %d\n\r", _lineNr);
        log(_title);
        log("\n\rApplication stopped!\n\r");
        blinkNTimes(5, 200);
    } while (false);
#else
    blinkNTimes(5, 200);
#endif
}

auto myabs(int val)
{
    return val < 0 ? -val : val;
}

auto magnet_vector_xy_abs(const Magnetometer::Vec3& magn_vec)
{
    return (myabs(std::get<Magnetometer::X>(magn_vec)) + myabs(std::get<Magnetometer::Y>(magn_vec))) / 2;
}

auto check_if_triggered(unsigned threshold, int vec_abs_val)
{
    return vec_abs_val > threshold;
}

auto check_if_direction_match(const Magnetometer::Vec3&)
{
    // TODO
    return true;
}

void mainer()
{
    log("Start program\n\r");
    blinkNTimes(3);

#ifndef MAGNETOMETER
    log("Initialize radio\n\r");
    Communication::SpiMaster spi{hspi1};
    Radio::nRF24 radio{
        spi,
        Gpio::BinaryIO{SPI1_nRF24_CS_GPIO_Port, SPI1_nRF24_CS_Pin},
        Gpio::BinaryIO{RADIO_ENABLE_GPIO_Port, RADIO_ENABLE_Pin}};

    if (!radio.init())
    {
        ErrorStop("Radio's init failed", __LINE__);
    }
    else
    {
        log("Radio initialized!\n\r");
        blinkNTimes(2);
    }
#endif    // MAGNETOMETER_TEST

#if defined(TRANSMITTER)
    log("Transmitter specific init\n\r");

#ifndef MAGNETOMETER
    /* rfid init */
    log("Init rfid\n\r");
    RfidTransceiver<200> rfid{huart1, RFID_CODE1};
    // auto rfidCall = make_callback([&rfid]() { rfid.idleInterrupt(); });
    // register_uart1Idle(&rfidCall);
    // USART1->CR1 |= USART_CR1_IDLEIE | USART_CR1_UE; //
    // rfid.init();

    /* button init */
    log("Init button\n\r");
    // nothing to do
#endif    // MAGNETOMETER_TEST

    /* magnet sensor init */
    log("Init magnets\n\r");
    auto magn = Magnetometer(hi2c1,
                             Magnetometer::Range::gauss2,
                             Magnetometer::Rate::hz25,
                             Magnetometer::Mode::continuous_conv);
    if (!magn.initialize())
        ErrorStop("Unable to initialize magnetometer", __LINE__);

#ifndef MAGNETOMETER
    log("Opening writing pipe\n\r");
    if (!radio.openWritingPipe({0x00, 0x01, 0x02, 0x03, 0x04}))
        ErrorStop("Opening writing pipe failed", __LINE__);

    if (!radio.setPowerAplification(Radio::nRF24::PALevel::dbm0))
        ErrorStop("Setting power failed", __LINE__);

    if (!radio.stopListening())
        ErrorStop("Listening stop failed", __LINE__);

    if (!radio.enableTransmitWithoutAck())
        ErrorStop("No ack transmission enable failed", __LINE__);
#endif    // MAGNETOMETER_TEST

#endif    // transmitter init

#if defined(RECEIVER) and !defined(MAGNETOMETER)
    log("Receiver specific init\n\r");

    HAL_GPIO_WritePin(
        LAMP1_GPIO_Port,
        LAMP1_Pin,
        GPIO_PinState::GPIO_PIN_SET);    // set because of negative logic
    HAL_GPIO_WritePin(LAMP2_GPIO_Port, LAMP2_Pin, GPIO_PinState::GPIO_PIN_SET);

    if (!radio.openReadingPipe({0x00, 0x01, 0x02, 0x03, 0x04}, 1))
        ErrorStop("Opening reading pipe failed", __LINE__);
    if (!radio.setPowerAplification(
            Radio::nRF24::PALevel::dbm0))    // full power
        ErrorStop("Setting power failed", __LINE__);
    if (!radio.startListening())
        ErrorStop("Listeing start failed", __LINE__);

    log("Receiver init success\n\r");

#endif    // receiver init

    HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PinState::GPIO_PIN_SET);
    while (true)
    {

#if defined(TRANSMITTER)
#if not defined(MAGNETOMETER)
        /* BUTTON CODE */
        // bool state = HAL_GPIO_ReadPin(BUTTON_GPIO_Port, BUTTON_Pin);
        int64_t beginTime = HAL_GetTick();
        while (!HAL_GPIO_ReadPin(BUTTON_GPIO_Port, BUTTON_Pin))
        {
            ;
        }
        int64_t endTime = HAL_GetTick();
        if (MIN_BUTTON_PRESS_TIME_MS < (endTime - beginTime))
        {
            log("Button triggered\n\r");
            if (!radio.transmitData({KEY}))
                ErrorStop("Radio transmission failed", __LINE__);
            log("Msg sent\n\r");
            blinkNTimes(2);
        }
        /* END OF BUTTON CODE */

        /* RFID CODE */
        if (rfid.lampStart())
        {
            log("Rfid triggered\n\r");
            for (unsigned i = 0; i < RFID_ATTEMPTS_COUNT; ++i)
            {
                if (!radio.transmitData({KEY}))
                    ErrorStop("Radio transmission failed", __LINE__);
                log("Msg sent\n\r");
            }
            log("All rfid msgs sent\n\r");
            rfid.resetLampStart();
        }
        /* END OF RFID CODE */
#elif defined(MAGNETOMETER_TEST) and not defined(MAGNETOMETER_TRIGGER)    // MAGNETOMETER_TEST
        /* MAGNET CODE */
        auto magVec = magn.getMagneticVector_bare();
        if (!magVec.first)
        {
            log("Read of magnetic vector failed\n");
            continue;
        }
        else
        {
            log("X: %d, Y: %d, Z: %d\n",
                std::get<Magnetometer::X>(magVec.second),
                std::get<Magnetometer::Y>(magVec.second),
                std::get<Magnetometer::Z>(magVec.second));
        }
#elif defined(MAGNETOMETER_TRIGGER)
        auto magVec = magn.getMagneticVector_bare();
        if (!magVec.first)
        {
            log("Read of magnetic vector failed\n");
            continue;
        }
        auto absVal = magnet_vector_xy_abs(magVec.second);
        if (check_if_triggered(THRESHOLD_ABS_VEC, absVal))
        {
            log("Triggered!");
            
        }
#endif
#endif
        /* END OF MAGNET CODE */

#if defined(RECEIVER)
        auto radioCheck = radio.checkRxNotEmpty();
        if (radioCheck.first && radioCheck.second)
        {
            log("Message in fifo\n\r");
            auto data = radio.receiveData();
            if (!data.first)
            {
                log("Data receiving failed\n\r");
                continue;
            }
            else
            {
                log("Received data: %s", data.second.data());
            }
            if (data.second[0] == KEY)
            {
                log("Key accepted\n\r");
                if (!radio.stopListening())
                    log("Stopping radio error\n\r");
                HAL_GPIO_WritePin(LAMP1_GPIO_Port,
                                  LAMP1_Pin,
                                  GPIO_PinState::GPIO_PIN_RESET);
                HAL_GPIO_WritePin(LAMP2_GPIO_Port,
                                  LAMP2_Pin,
                                  GPIO_PinState::GPIO_PIN_SET);
                for (unsigned i = 0; i < TOGGLES_COUNT; ++i)
                {
                    HAL_GPIO_TogglePin(LAMP1_GPIO_Port, LAMP1_Pin);
                    HAL_GPIO_TogglePin(LAMP2_GPIO_Port, LAMP2_Pin);
                    HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
                    HAL_Delay(TOGGLE_INTERVAL_ms);
                }
                HAL_GPIO_WritePin(LAMP1_GPIO_Port,
                                  LAMP1_Pin,
                                  GPIO_PinState::GPIO_PIN_SET);
                HAL_GPIO_WritePin(LAMP2_GPIO_Port,
                                  LAMP2_Pin,
                                  GPIO_PinState::GPIO_PIN_SET);
                radio.flushRx();
                radio.clearFlags();
                log("Toggling ended\n\r");
                if (!radio.startListening())
                    log("Starting radio fail\n\r");
            }
            else
            {
                log("Key unknown\n\r");
            }
        }
        else if (!radioCheck.first)
        {
            log("Communication error!");
        }
#endif
    }
}
