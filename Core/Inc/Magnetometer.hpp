/**
 ******************************************************************************
 * @file    Magnetometer.h
 * @author  Michał Uznański
 * @date    Wed Jun 22 2022
 * @brief
 ******************************************************************************
 */

/* Define to prevent recursive inclusion ----------------------------------- */
#ifndef MAGNETOMETER_H_INCLUDED_
#define MAGNETOMETER_H_INCLUDED_

#include <stdint.h>
#include <tuple>
#include <utility>
#include <stm32f1xx_hal.h>
#include "Utilities.hpp"

class Magnetometer
{
    static constexpr unsigned OUT_X_LOW  = 0x8;
    static constexpr unsigned OUT_X_HIGH = 0x9;
    static constexpr unsigned OUT_Y_LOW  = 0xA;
    static constexpr unsigned OUT_Y_HIGH = 0xB;
    static constexpr unsigned OUT_Z_LOW  = 0xC;
    static constexpr unsigned OUT_Z_HIGH = 0xD;
    static constexpr unsigned WHO_AM_I   = 0xF;
    static constexpr unsigned CTRL5      = 0x24;
    static constexpr unsigned CTRL6      = 0x25;
    static constexpr unsigned CTRL7      = 0x26;

    static constexpr unsigned WHO_AM_I_VAL = 0b0100'1001;

    bool setRegister(uint8_t _addr, uint8_t _val)
    {
        auto ret = HAL_I2C_Mem_Write(&m_hi2c,
                                     m_i2cAddr,
                                     _addr,
                                     I2C_MEMADD_SIZE_8BIT,
                                     &_val,
                                     1,
                                     100);
        if (ret != HAL_OK)
        {
            debug("Write register failed\n");
            debug("ret val: %d\n", ret);
            debug("HAL i2c error code: %d\n", HAL_I2C_GetError(&m_hi2c));
        }
        return ret == HAL_OK;
    }

    std::pair<bool, uint8_t> readRegister(uint8_t _addr)
    {
        uint8_t data{};
        auto ret = HAL_I2C_Mem_Read(&m_hi2c,
                                    m_i2cAddr,
                                    _addr,
                                    I2C_MEMADD_SIZE_8BIT,
                                    &data,
                                    1,
                                    100);
        if (ret != HAL_OK)
        {
            debug("Read register failed\n");
            debug("ret val: %d\n", ret);
            debug("HAL i2c error code: %d\n", HAL_I2C_GetError(&m_hi2c));
        }
        return std::make_pair(ret == HAL_OK, data);
    }

public:
    using x    = int16_t;
    using y    = int16_t;
    using z    = int16_t;
    using Vec3 = std::tuple<x, y, z>;    // x, y, z
    enum class Range
    {
        gauss2 = 0,
        gauss4,
        gauss8,
        gauss12,
    };

    enum class Rate
    {
        hz3_125 = 0,
        hz6_25,
        hz12_5,
        hz25,
        hz50,
        hz100,      // some shit to use this rate, dont
        dont_use    // it probably means - turn off
    };

    enum class Mode
    {
        continuous_conv = 0,
        single_conv,
        power_down,
        power_down1    // the same as above, but another value in reg
    };

    enum Idcs
    {
        X,
        Y,
        Z
    };

    Magnetometer(I2C_HandleTypeDef& _hi2c,
                 Range _range     = Range::gauss4,
                 Rate _rate       = Rate::dont_use,
                 Mode _mode       = Mode::power_down1,
                 uint8_t _i2cAddr = 0b0011101)
        : m_i2cAddr(_i2cAddr << 1), m_currRange{_range}, m_currRate(_rate),
          m_currMode(_mode), m_hi2c(_hi2c)
    {
    }
    ~Magnetometer() = default;

    bool setRange(Range _r)
    {
        if (setRegister(CTRL6, static_cast<unsigned>(_r) << 5))
            m_currRange = _r;
        else
            return false;
        return true;
    }
    bool setRate(Rate _r)
    {
        auto regVal = readRegister(CTRL5);
        if (!regVal.first)
            return false;
        regVal.second &= ~(0b111 << 2);
        regVal.second |= static_cast<uint8_t>(_r) << 2;
        if (setRegister(CTRL5, regVal.second))
            m_currRate = _r;
        else
            return false;
        return true;
    }

    bool setMode(Mode _m)
    {
        auto regVal = readRegister(CTRL7);
        if (!regVal.first)
            return false;
        regVal.second &= ~(0b11 << 0);
        regVal.second |= static_cast<uint8_t>(_m) << 0;
        if (setRegister(CTRL7, regVal.second))
            m_currMode = _m;
        else
            return false;
        return true;
    }

    bool initialize()
    {
        if (!checkConnection())
        {
            log("check connection failed");
            return false;
        }
        if (!setRange(m_currRange))
        {
            log("setting range failed");
            return false;
        }
        if (!setRate(m_currRate))
        {
            log("setting rate failed");
            return false;
        }
        if (!setMode(m_currMode))
        {
            log("setting mode failed");
            return false;
        }
        return true;
    }

    std::pair<bool, Vec3> getMagneticVector_bare()
    {
        Vec3 retVec{};
        bool state;
        auto xl = readRegister(OUT_X_LOW);
        auto xh = readRegister(OUT_X_HIGH);
        auto yl = readRegister(OUT_Y_LOW);
        auto yh = readRegister(OUT_Y_HIGH);
        auto zl = readRegister(OUT_Z_LOW);
        auto zh = readRegister(OUT_Z_HIGH);
        if (!xl.first || !xh.first || !yl.first || !yh.first || !zl.first ||
            !zh.first)
            state = false;
        else
        {
            std::get<0>(retVec) = xl.second | (xh.second << 8);
            std::get<1>(retVec) = yl.second | (yh.second << 8);
            std::get<2>(retVec) = zl.second | (zh.second << 8);
            state               = true;
        }

        return std::make_pair(state, retVec);
    }

    bool checkConnection()
    {
        auto ret = readRegister(WHO_AM_I);
        if (!ret.first)
            return false;
        if (ret.second != WHO_AM_I_VAL)
        {
            log("Who am I not consistent");
            log("Got: %d, anticipated: %d\n", ret.second, WHO_AM_I_VAL);
            return false;
        }
        return true;
    }

private:
    const uint8_t m_i2cAddr;
    Range m_currRange;
    Rate m_currRate;
    Mode m_currMode;
    I2C_HandleTypeDef& m_hi2c;
};

#endif /* MAGNETOMETER_H_INCLUDED_ */

/**** END OF FILE ****/
