/*
 * Callback.hpp
 *
 *  Created on: Apr 23, 2022
 *      Author: Dell
 */

#ifndef INC_CALLBACK_HPP_
#define INC_CALLBACK_HPP_
extern "C" void __cxa_pure_virtual();
void operator delete(void*, unsigned int);
class PraCallback
{
public:
    PraCallback()             = default;
    virtual ~PraCallback()    = default;
    virtual void operator()() = 0;
};

template<class Functionoid>
class Callback : public PraCallback
{
public:
    Callback(const Functionoid& _function) : m_function{_function}
    {
    }
    virtual ~Callback() = default;
    virtual void operator()() override
    {
        m_function();
    }

private:
    Functionoid m_function;
};

template<class Functionoid>
auto make_callback(const Functionoid& _function)
{
    return Callback<Functionoid>(_function);
}

#endif /* INC_CALLBACK_HPP_ */
