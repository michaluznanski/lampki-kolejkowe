/*
 * RfidTransceiver.h
 *
 *  Created on: Apr 23, 2022
 *      Author: Dell
 */

#ifndef INC_RFIDTRANSCEIVER_H_
#define INC_RFIDTRANSCEIVER_H_

#include "Utilities.hpp"

#include "stm32f1xx_hal.h"
#include <algorithm>

template<unsigned BUF_SIZE>
class RfidTransceiver
{
public:
    RfidTransceiver(UART_HandleTypeDef& _uart, const uint8_t* _rfidCode)
        : mp_rfidCode{_rfidCode}, m_uart{_uart}, m_dmaBuffer{0}, m_lampStart{
                                                                     false}
    {
    }

    ~RfidTransceiver() = default;

    void init()
    {
        HAL_UART_Receive_DMA(&m_uart, m_dmaBuffer, BUF_SIZE);
    }

    void idleInterrupt()
    {
        HAL_UART_DMAStop(&m_uart);
        unsigned recvBytes = BUF_SIZE - m_uart.hdmarx->Instance->CNDTR;
        log("Rfid irq\n\r");
        if (std::equal(m_dmaBuffer, m_dmaBuffer + recvBytes, mp_rfidCode))
        {
            log("Rfid code correct\n\r");
            m_lampStart = true;
        }
        else
        {
            log("Rfid code incorrect\n\r");
        }
        HAL_UART_Receive_DMA(&m_uart, m_dmaBuffer, BUF_SIZE);
    }

    bool lampStart() const
    {
        return m_lampStart;
    }

    void resetLampStart()
    {
        m_lampStart = false;
    }

private:
    const uint8_t* mp_rfidCode;
    UART_HandleTypeDef& m_uart;
    uint8_t m_dmaBuffer[BUF_SIZE];
    volatile bool m_lampStart;
};

#endif /* INC_RFIDTRANSCEIVER_H_ */
