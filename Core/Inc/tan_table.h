#include <cstdint>
#include <cmath>


// tangensy od 0 st. do 89 st. pomnożone razy 1000
static constexpr int tan_times_1000[] = {0, 17, 35, 52, 70, 87, 105, 123, 141, 158, 176, 194, 213, 231, 249, 268, 287, 306, 325, 344, 364, 384, 404, 424, 445, 466, 488, 510, 532, 554, 577, 601, 625, 649, 675, 700, 727, 754, 781, 810, 839, 869, 900, 933, 966, 1000, 1036, 1072, 1111, 1150, 1192, 1235, 1280, 1327, 1376, 1428, 1483, 1540, 1600, 1664, 1732, 1804, 1881, 1963, 2050, 2145, 2246, 2356, 2475, 2605, 2747, 2904, 3078, 3271, 3487, 3732, 4011, 4331, 4705, 5145, 5671, 6314, 7115, 8144, 9514, 11430, 14301, 19081, 28636, 57290};

// x to przyprostokątna, y przeciw, czyli tan = y / x
template <typename T>
inline int atan(T x, T y)
{
    // szukamy kąta
    int tan_1000 = (1000 * abs(y)) / abs(x);
    int kat = 0;
    for (; kat < 90; ++kat)
    {
        if (tan_times_1000[kat] > tan_1000)
            break;
    }
    // szukamy ćwiartki
    if (x >= 0 and y >= 0)
    {
        return kat;
    }
    if (x < 0 and y >= 0)
    {
        return 180 - kat;
    }
    if (x < 0 and y < 0)
    {
        return 180 + kat;
    }
    if (x >= 0 and y < 0)
    {
        return 360 - kat;
    }
    return -361;
}
