/**
 ******************************************************************************
 * @file    BinaryIO.h
 * @author  Michał Uznański
 * @date    Wed May 11 2022
 * @brief
 ******************************************************************************
 */

/* Define to prevent recursive inclusion ----------------------------------- */
#ifndef BINARYIO_H_INCLUDED_
#define BINARYIO_H_INCLUDED_

#include <stdint.h>
#include <utility>
#include "stm32f103xb.h"
#include "stm32f1xx_hal.h"

namespace Gpio
{
using GPIO_PinType = uint16_t;
typedef std::pair<GPIO_TypeDef*, GPIO_PinType> BinaryIO;
}    // namespace Gpio

#endif /* BINARYIO_H_INCLUDED_ */

/**** END OF FILE ****/
