/**
 ******************************************************************************
 * @file    nRF24.hpp
 * @author  Michał Uznański
 * @date    Wed May 11 2022
 * @brief
 ******************************************************************************
 */

/* Define to prevent recursive inclusion ----------------------------------- */
#ifndef NRF24_HPP_INCLUDED_
#define NRF24_HPP_INCLUDED_

#include <cstdint>
#include <bitset>
#include <tuple>
#include <utility>

#include "SpiMaster.hpp"
#include "BinaryIO.hpp"
#include "Utilities.hpp"

namespace Radio
{
/**
 * @brief
 *
 */
class nRF24
{
public:
    enum class Command : uint8_t
    {
        // R_REGISTER   = 0b000AAAAA,
        R_REGISTER = 0b00000000,
        // W_REGISTER   = 0b001AAAAA,
        W_REGISTER   = 0b00100000,
        R_RX_PAYLOAD = 0b01100001,
        W_TX_PAYLOAD = 0b10100000,
        FLUSH_TX     = 0b11100001,
        FLUSH_RX     = 0b11100010,
        REUSE_TX_PL  = 0b11100011,
        R_RX_PL_WID  = 0b01100000,
        // W_ACK_PAYLOAD= 0x10101PPP, // three last semibytes refer to number
        // of pipeline
        W_ACK_PAYLOAD =
            0b10101000,    // to select pipeline insert its nr to mentioned bits
        W_TX_PAYLOAD_NOACK = 0b10110000,
        NOP                = 0b11111111    // useful for status read
    };

    enum Registers : uint8_t
    {
        CONFIG = 0X0,
        EN_AA,
        EN_RXADDR,
        SETUP_AW,
        SETUP_RETR,
        RF_CH,
        RF_SETUP,
        STATUS,
        OBSERVE_TX,
        RPD,
        RX_ADDR_P0,
        RX_ADDR_P1,
        RX_ADDR_P2,
        RX_ADDR_P3,
        RX_ADDR_P4,
        RX_ADDR_P5,
        TX_ADDR,
        RX_PW_P0,
        RX_PW_P1,
        RX_PW_P2,
        RX_PW_P3,
        RX_PW_P4,
        RX_PW_P5,
        FIFO_STATUS,
        DYNPD = 0X1C,
        FEATURE
    };

    template<unsigned SIZE>
    using ByteArray = std::array<uint8_t, SIZE>;
    struct Status
    {
        bool rxDataReady;
        bool txDataSent;
        bool maxRetransmissionAttempt;
        std::bitset<3> dataPipeNr;
        bool txFull;
    };

    nRF24(Communication::SpiMaster& _spi,
          Gpio::BinaryIO&& _csn,
          Gpio::BinaryIO&& _ce)

        : m_spi{_spi}, m_csn{_csn}, m_ce{_ce}
    {
    }

    ~nRF24() = default;

    bool init()
    {
        // TODO poprawić inita
        // set to standby mode
        HAL_GPIO_WritePin(m_ce.first,
                          m_ce.second,
                          GPIO_PinState::GPIO_PIN_RESET);
        HAL_GPIO_WritePin(m_csn.first,
                          m_csn.second,
                          GPIO_PinState::GPIO_PIN_SET);
        HAL_Delay(5);
        setSpeed(Speed::kbps250);
        setPowerAplification(PALevel::dbm0);
        setAutoRetransmission(ARDelay::us2000, 15);
        // TODO feature
        // TODO dynamic payload
        // TODO payload with ACK
        // nie za bardzo wiem jak powyższe działają
        setAddrWidth(5);
        setChannel(10);
        for (unsigned i = 0; i <= 5; ++i)
        {
            enableAutoAck(i);
            setPipePayloadSize(i, 32);
        }
        clearFlags();
        flushRx();
        flushTx();
        enableCrc(true);
        setCrc(CrcLen::byte2);
        m_isInitialized = setPowerUp();
        return m_isInitialized;
    }

    bool enableAutoAck(uint8_t _pipeNr, bool _newState = true)
    {
        auto readRet = readRegister(Registers::EN_AA);
        if (readRet.first)
        {
            uint8_t newVal = readRet.second;
            newVal =
                _newState ? newVal | (1 << _pipeNr) : newVal & ~(1 << _pipeNr);
            auto writeRet = writeRegister(Registers::EN_AA, newVal);
            return writeRet;
        }
        return false;
    }

    bool enableTransmitWithoutAck(bool _newState = true)
    {
        auto readRet = readRegister(Registers::FEATURE);
        if (readRet.first)
        {
            uint8_t newVal = readRet.second;
            if (_newState)
            {
                newVal |= 1 << 0;
            }
            else
            {
                newVal &= ~(1 << 0);
            }
            auto writeRet = writeRegister(Registers::FEATURE, newVal);
            return writeRet;
        }
        return false;
    }

    enum class Speed
    {
        mbps1 = 0x0,
        mbps2 = 0x1,
        kbps250
    };
    bool setSpeed(Speed _speed)
    {
        auto readRet = readRegister(Registers::RF_SETUP);
        if (readRet.first)
        {
            uint8_t newVal{0};
            if (_speed != Speed::kbps250)
            {
                newVal = (readRet.second & ~(0b11 << 3)) |
                         (static_cast<uint8_t>(_speed) << 3);
                newVal &= ~(1 << 5);
            }
            else
            {
                newVal = readRet.second | (1 << 5);
            }
            auto writeRet = writeRegister(Registers::RF_SETUP, newVal);
            return writeRet;
        }
        return false;
    }

    enum class PALevel
    {
        dbm18 = 0x0,
        dbm12 = 0x1,
        dbm6  = 0x2,
        dbm0  = 0x3,
    };
    bool setPowerAplification(PALevel _pa)
    {
        auto readRet = readRegister(Registers::RF_SETUP);
        if (readRet.first)
        {
            uint8_t newVal = (readRet.second & ~(0b11 << 1)) |
                             (static_cast<uint8_t>(_pa) << 1);
            auto writeRet = writeRegister(Registers::RF_SETUP, newVal);
            return writeRet;
        }
        return false;
    }
    enum class ARDelay
    {
        us250 = 0x0,
        us500,
        us750,
        us1000,
        us1250,
        us1500,
        us1750,
        us2000,
        us2250,
        us2500,
        us2750,
        us3000,
        us3250,
        us3500,
        us3750,
        us4000
    };
    bool setAutoRetransmission(ARDelay _delay, uint8_t _arCount)
    {
        auto readRet = readRegister(Registers::SETUP_RETR);
        if (readRet.first)
        {
            uint8_t newVal = static_cast<uint8_t>(_delay) << 4;
            newVal |= _arCount;
            auto writeRet = writeRegister(Registers::SETUP_RETR, newVal);
            return writeRet;
        }
        return false;
    }
    bool setPipePayloadSize(uint8_t _pipeNr = 0, uint8_t _size_bytes = 32)
    {
        if (_size_bytes > 32)
        {
            return false;
        }
        auto writeRet =
            writeRegister(Registers::RX_PW_P0 + _pipeNr, _size_bytes);
        return writeRet;
    }
    bool setAddrWidth(uint8_t _addrWidth)
    {
        bool ret{false};
        if (_addrWidth <= 5 && _addrWidth >= 3)
        {
            ret = writeRegister(Registers::SETUP_AW, (uint8_t)(_addrWidth - 2));
        }
        return ret;
    }
    bool setChannel(uint8_t _channel)
    {
        return writeRegister(Registers::RF_CH,
                             (uint8_t)(_channel & (0b1111111)));
    }
    bool clearFlags()
    {
        return writeRegister(Registers::STATUS, (1 << 6) | (1 << 5) | (1 << 4));
    }
    bool flushTx()
    {
        return std::get<bool>(sendCommand(Command::FLUSH_TX));
    }
    bool flushRx()
    {
        return std::get<bool>(sendCommand(Command::FLUSH_RX));
    }
    enum class CrcLen
    {
        byte1 = 0x0,
        byte2
    };
    bool setCrc(CrcLen _len)
    {
        auto readRet = readRegister(Registers::CONFIG);
        if (readRet.first)
        {
            uint8_t newVal = readRet.second;
            newVal &= ~(0b1 << 2);
            newVal |= (static_cast<uint8_t>(_len)) << 2;
            auto writeRet = writeRegister(Registers::CONFIG, newVal);
            return writeRet;
        }
        return false;
    }
    bool enableCrc(bool _newState)
    {
        auto readRet = readRegister(Registers::CONFIG);
        if (readRet.first)
        {
            uint8_t newVal = readRet.second;
            if (_newState)
            {
                newVal |= 1 << 3;
            }
            else
            {
                newVal &= ~(1 << 3);
            }
            auto writeRet = writeRegister(Registers::CONFIG, newVal);
            return writeRet;
        }
        return false;
    }
    bool setPowerDown()
    {
        auto readRet = readRegister(Registers::CONFIG);
        if (readRet.first)
        {
            uint8_t newVal = readRet.second;
            newVal &= ~(1 << 1);
            auto writeRet = writeRegister(Registers::CONFIG, newVal);
            return writeRet;
        }
        return false;
    }
    bool setPowerUp()
    {
        auto readRet = readRegister(Registers::CONFIG);
        if (readRet.first)
        {
            uint8_t newVal = readRet.second;
            newVal |= (1 << 1);
            auto writeRet = writeRegister(Registers::CONFIG, newVal);
            return writeRet;
        }
        return false;
    }
    bool enablePipe(uint8_t _pipeNr, bool _newState = true)
    {
        auto readRet = readRegister(Registers::EN_RXADDR);
        if (readRet.first)
        {
            uint8_t newVal = readRet.second;
            if (_newState)
            {
                newVal |= (1 << _pipeNr);
            }
            else
            {
                newVal &= ~(1 << _pipeNr);
            }
            auto writeRet = writeRegister(Registers::EN_RXADDR, newVal);
            return writeRet;
        }
        return false;
    }

    bool dumpRegister(uint8_t _reg)
    {
        auto ret = readRegister(_reg);
        log("Register dump:\n\r");
        log("Addr: 0x%.2x Value: 0x%.2x \n\r", _reg, ret.second);
        return ret.first;
    }

    bool dumpRegister(uint8_t _reg, uint8_t _val)
    {
        log("Register dump:\n\r");
        log("Addr: 0x%.2x Value: 0x%.2x \n\r", _reg, _val);
        return true;
    }

    template<unsigned PAYLOAD_LEN = 32>
    bool transmitData(ByteArray<PAYLOAD_LEN> _data,
                      Command _command = Command::W_TX_PAYLOAD)
    {
        bool state{false};
        clearFlags();
        auto ret = sendCommand(_command, _data);
        HAL_GPIO_WritePin(m_ce.first, m_ce.second, GPIO_PIN_SET);
        HAL_Delay(5);    // need to switch to active state
        while (std::get<bool>(ret))
        {
            log("WAIT FOR TRANSFER COMPLETE\n\r");
            log("Status dump\n\r");
            dumpRegister(Radio::nRF24::Registers::STATUS);

            updateStatus();
            if (m_status.maxRetransmissionAttempt)
            {
                log("Max transmission attempts reached - message not sent\n\r");
                flushTx();
                break;
            }
            else if (m_status.txDataSent)
            {
                log("Message sent\n\r");
                state = true;
                break;
            }
        }
        HAL_GPIO_WritePin(m_ce.first, m_ce.second, GPIO_PIN_RESET);
        return state;
    }

    template<unsigned PAYLOAD_LEN = 32>
    std::pair<bool, ByteArray<PAYLOAD_LEN>> receiveData()
    {
        clearFlags();
        auto ret = sendCommand(Command::R_RX_PAYLOAD, ByteArray<PAYLOAD_LEN>{});
        return make_pair(std::get<bool>(ret),
                         std::get<ByteArray<PAYLOAD_LEN>>(ret));
    }

    /**
     * @brief
     * @note First to read pipes may have 5 bytes address, while the rest has
     * one own address byte, LSB, the rest is the same like in pipeline nr 1
     *
     * @param _addr
     * @param _pipeNr
     * @return true
     * @return false
     */
    bool openReadingPipe(ByteArray<5> _addr, uint8_t _pipeNr = 0)
    {
        auto ret = sendCommand((Command)((uint8_t)Command::W_REGISTER |
                                         (Registers::RX_ADDR_P0 + _pipeNr)),
                               _addr);    // consider endianness issue
        enablePipe(_pipeNr);
        return std::get<bool>(ret);
    }

    bool openWritingPipe(ByteArray<5> _addr)
    {
        auto ret = sendCommand(
            (Command)((uint8_t)Command::W_REGISTER | Registers::TX_ADDR),
            _addr);
        return std::get<bool>(ret);
    }

    bool updateStatus()
    {
        auto ret = sendCommand(
            Command::NOP);    // sendCommand's side effect is updating status
        return std::get<bool>(ret);
    }

    [[nodiscard]] const Status& getStatus() const
    {
        return m_status;
    }

    bool startListening()
    {
        bool state{false};
        auto ret = readRegister(0x00);
        // TODO dodać mapę rejestrów, by nie trzeba było ich za każdym razem
        // odczytywać
        if (ret.first)
        {
            /**
             * @brief if old content known - overwrite. | 1 because we want to
             * set PRIM_RX in Config register to set the device to receiving
             */
            uint8_t newVal = ret.second | 1;
            auto writeRet =
                writeRegister(Registers::CONFIG /*config register*/, newVal);
            // TODO dodać zerowanie flag statusu
            if (writeRet)
            {
                HAL_GPIO_WritePin(m_ce.first,
                                  m_ce.second,
                                  GPIO_PinState::GPIO_PIN_SET);
                state = true;
            }
        }
        return state;
    }

    bool stopListening()
    {
        bool retState{false};
        HAL_GPIO_WritePin(m_ce.first,
                          m_ce.second,
                          GPIO_PinState::GPIO_PIN_RESET);
        auto readState = readRegister(0x00);
        if (readState.first)
        {
            uint8_t newVal  = readState.second & (~1);
            auto writeState = writeRegister(Registers::CONFIG, newVal);
            if (writeState)
            {
                retState = true;
            }
        }
        return retState;
    }

    /**
     * @brief Returns nr of pipe where message is pending. If no message is
     * pending, 0b111 is returned.
     */
    auto checkRxNotEmpty()
    {
        auto ret = updateStatus();
        log("Status register:\n\r");
        dumpRegister(Registers::STATUS);
        return std::make_pair(ret, getStatus().rxDataReady);
    }

    // template<unsigned SIZE>
    // std::pair<bool, ByteArray<SIZE>> receiveData()
    // {
    //     auto readState = sendCommand(Command::R_RX_PL_WID, 0x0);
    //     if (std::get<bool>(readState) && m_status.rxDataReady)
    //     {
    //         auto data = sendCommand(
    //             Command::R_RX_PAYLOAD,
    //             std::get<2>(
    //                 readState)[0]);    // TODO dodać sendCommand z
    //                 możliwością
    //                                    // określenia ile gównodanych ma
    //                                    zostać
    //                                    // przepchniętych
    //         return std::make_pair(std::get<bool>(data),
    //                               std::get<ByteArray<SIZE>>(data));
    //     }
    // }

private:
    constexpr Status u8ToStatus(uint8_t _byte)
    {
        Status s{};
        s.rxDataReady = (_byte >> 6) & 1;    // TODO wywalić zaczarowane liczby
        s.txDataSent  = (_byte >> 5) & 1;
        s.maxRetransmissionAttempt = (_byte >> 4) & 1;
        s.dataPipeNr               = (_byte >> 1) & 0b111;
        s.txFull                   = (_byte >> 0) & 1;
        return s;
    }

    template<unsigned SIZE>
    std::tuple<bool, Status, std::array<uint8_t, SIZE>>
    sendCommand(Command _command, std::array<uint8_t, SIZE> _args)
    {
        std::array<uint8_t, SIZE> retArr{};
        ByteArray<SIZE + sizeof(Command)> sendArr{};
        sendArr[0] = static_cast<uint8_t>(_command);
        std::copy(_args.cbegin(), _args.cend(), sendArr.begin() + 1);
        auto ret = m_spi.transfer<SIZE + sizeof(Command)>(sendArr, m_csn);

        /* Debug stuff */
        // log("sendCommand retVals:\n\r");
        // log("bool : ");
        // ret.first ? log("true\n\r") : log("false\n\r");
        // for (auto value : ret.second)
        // {
        //     log("%x ", value);
        // }
        // log("\n\rEnd of frame\n\r");
        /* End of debug stuff */

        if (ret.first)
        {
            m_status = u8ToStatus(ret.second[0]);
            std::copy(ret.second.cbegin() + 1,
                      ret.second.cend(),
                      retArr.begin());
        }
        return std::make_tuple(ret.first, m_status, retArr);
    }

    template<typename... T>
    std::tuple<bool, Status, std::array<uint8_t, sizeof...(T)>>
    sendCommand(Command _command,
                T... _args)    // TODO przetestuj, czy kolejność bitów i
                               // bajtów się zgadza
    {
        return sendCommand<sizeof...(T)>(_command, {_args...});
    }

    std::pair<bool, uint8_t>
    readRegister(uint8_t _addr)    // TODO addr powinien być enumem
    {
        char _;
        auto regRead =
            sendCommand((Command)((uint8_t)Command::R_REGISTER | _addr), _);
        // log("Read register dump\n\r");
        // dumpRegister(_addr, std::get<2>(regRead)[0]);
        return std::make_pair(std::get<bool>(regRead), std::get<2>(regRead)[0]);
    }

    template<typename T>
    std::pair<bool, ByteArray<sizeof(T)>> readRegister(uint8_t _addr)
    {
        constexpr ByteArray<sizeof(T)> readArr{};
        auto regRead =
            sendCommand((Command)((uint8_t)Command::W_REGISTER | _addr),
                        readArr);
        // log("Read register dump\n\r");
        // dumpRegister(_addr, std::get<2>(regRead)[0]);
        return std::make_pair(std::get<bool>(regRead), std::get<2>(regRead));
    }

    template<typename T>
    bool writeRegister(uint8_t _addr, T _val)
    {
        auto* p = reinterpret_cast<uint8_t*>(&_val);
        ByteArray<sizeof(T)> writeArr{};
        for (unsigned i = 0; i < sizeof(T); ++i)
        {
            writeArr[i] = p[i];
        }
        auto ret = sendCommand((Command)((uint8_t)Command::W_REGISTER | _addr),
                               writeArr);

        log("Write register dump\n\r");
        dumpRegister(_addr);

        return std::get<bool>(ret);
    }

    Communication::SpiMaster& m_spi;
    Status m_status{};
    Gpio::BinaryIO m_csn;
    Gpio::BinaryIO m_ce;
    bool m_isInitialized{false};
    // uint8_t m_readPayloadSize[5];    // TODO 5 to liczba rur
    // uint8_t m_writePayloadSize;
};
}    // namespace Radio

#endif /* NRF24_HPP_INCLUDED_ */

/**** END OF FILE ****/
