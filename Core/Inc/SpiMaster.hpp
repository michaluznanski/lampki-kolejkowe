/**
 ******************************************************************************
 * @file    SpiMaster.h
 * @author  Michał Uznański
 * @date    Wed May 11 2022
 * @brief
 ******************************************************************************
 */

/* Define to prevent recursive inclusion ----------------------------------- */
#ifndef SPIMASTER_H_INCLUDED_
#define SPIMASTER_H_INCLUDED_

#include <cstdint>
#include <utility>
#include <array>
#include <cassert>

#include "BinaryIO.hpp"

/* HAL shits */
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_def.h"
#include "stm32f1xx_hal_gpio.h"
#include "stm32f1xx_hal_spi.h"

namespace Communication
{
/**
 * @brief
 *
 */
class SpiMaster
{
public:
    template<typename... T>
    explicit constexpr SpiMaster(SPI_HandleTypeDef& _hspi,
                                 unsigned _timeout_ms = 50)
        : m_spi{&_hspi}, m_timeout_ms{_timeout_ms}
    {
    }
    ~SpiMaster() = default;

    template<unsigned SIZE>
    std::pair<bool, std::array<uint8_t, SIZE>>
    transfer(std::array<uint8_t, SIZE>& _bytes, const Gpio::BinaryIO& _cs)
    {
        std::array<uint8_t, SIZE> _retBytes;
        HAL_GPIO_WritePin(_cs.first, _cs.second, GPIO_PinState::GPIO_PIN_RESET);
        auto status = HAL_SPI_TransmitReceive(m_spi,
                                              _bytes.data(),
                                              _retBytes.data(),
                                              SIZE,
                                              m_timeout_ms);
        HAL_GPIO_WritePin(_cs.first, _cs.second, GPIO_PinState::GPIO_PIN_SET);
        return std::make_pair(status == HAL_OK, _retBytes);
    }

private:
    SPI_HandleTypeDef* m_spi;
    unsigned m_timeout_ms;
};
}    // namespace Communication

#endif /* SPIMASTER_H_INCLUDED_ */

/**** END OF FILE ****/
