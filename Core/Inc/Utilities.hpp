/*
 * Utilities.h
 *
 *  Created on: 27 kwi 2022
 *      Author: Dell
 */

#ifndef INC_UTILITIES_H_
#define INC_UTILITIES_H_

#include "stm32f1xx_hal.h"
#include "usart.h"
#include <cstring>
#include <cstdio>

/*
 * @brief simple debug logger using UART2 with HAL libraries
 */
template<typename... T>
inline void log(const char* _message, T... _args)
{
    constexpr unsigned BUF_SIZE   = 200;
    constexpr unsigned TIMEOUT_ms = 200;
    char buffer[BUF_SIZE];
    snprintf(buffer, BUF_SIZE, _message, _args...);
    HAL_UART_Transmit(&huart2, (uint8_t*)buffer, strlen(buffer), TIMEOUT_ms);
}

template<typename... T>
inline void debug(const char* _message, T... _args)
{
#ifdef DEBUG
    log(_message, _args...);
#endif
}

#endif /* INC_UTILITIES_H_ */
